import random
import string

from django.shortcuts import render
from django.core.files.storage import FileSystemStorage
from staff_app.settings import ALLOWED_EXTENSIONS, BASE_DIR, MEDIA_ROOT, MEDIA_URL

from .models import Log, User


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def format_data_staff(record_staff):
    staff = {
        'staff': {
            'id': record_staff.id,
            'name': record_staff.name,
            'gender': record_staff.gender,
            'avatar': record_staff.avatar
        }
    }
    return staff


def auto_generate_anything():
    name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(12))
    return name


def index(request):
    index_url = str(BASE_DIR) + '/staff/template/user/info.html'
    return render(request, index_url, status=200)


def submit(request):
    info_url = str(BASE_DIR) + '/staff/template/user/info.html'

    name = request.POST.get('name')
    gender = request.POST.get('gender')
    avatar = request.FILES.get('avatar')

    if not name:
        return render(request, info_url, {'message': 'Name is empty!'}, status=400)

    if not gender:
        return render(request, info_url, {'message': 'Gender is empty!'}, status=400)

    if not avatar:
        return render(request, info_url, {'message': 'Avatar is empty!'}, status=400)

    if avatar and allowed_file(avatar.name):
        record_staff = User().create_update_user(name, gender)

        fss = FileSystemStorage()
        file = fss.save(avatar.name, avatar)
        file_url = fss.url(file)

        record_staff.create_update_user(name, gender, file_url)

        staff = format_data_staff(record_staff)

        record_log = Log().create_update_log('Create new staff information!', 'Staff')

        return render(request, info_url, staff, status=200)

    return render(request, info_url, {'message': 'Upload avatar failed!'}, status=400)


def update(request, staff_id):
    info_url = str(BASE_DIR) + '/staff/template/user/info.html'

    record_staff = User.objects.filter(id=staff_id).first()

    if not record_staff:
        return render(request, info_url, {'message': 'User does not exist!'}, status=400)

    staff = format_data_staff(record_staff)

    name = request.POST.get('name') if request.POST.get('name') is not None else record_staff.name
    gender = request.POST.get('gender') if request.POST.get('gender') is not None else record_staff.gender
    avatar = request.FILES.get('avatar')

    if not name:
        return render(request, info_url, {'message': 'Name is empty!'})

    if not gender:
        return render(request, info_url, {'message': 'Gender is empty!'})

    file_url = ''
    if avatar and allowed_file(avatar.name):
        fss = FileSystemStorage()
        file = fss.save(avatar.name, avatar)
        file_url = fss.url(file)

    avatar = file_url if file_url else record_staff.avatar

    record_staff.create_update_user(name, gender, avatar)

    staff = format_data_staff(record_staff)

    record_log = Log().create_update_log('Update staff information!', 'Staff')

    return render(request, info_url, staff, status=200)


def display_all_staff(request):
    list_staff_url = str(BASE_DIR) + '/staff/template/user/list_staff.html'

    records_staff = User.objects.all()

    data_staff = []
    for data_record in records_staff:
        if data_record:
            data = {
                'id': data_record.id,
                'name': data_record.name,
                'gender': data_record.gender,
                'avatar': data_record.avatar
            }
            data_staff.append(data)

    staff = {
        'staff': data_staff
    }

    return render(request, list_staff_url, staff, status=200)


def show_info(request, staff_id):
    index_url = str(BASE_DIR) + '/staff/template/user/index.html'

    record_staff = User.objects.filter(id=staff_id).first()

    if not record_staff:
        return render(request, index_url, {'message': 'User does not exist!'}, status=400)

    staff = format_data_staff(record_staff)

    record_log = Log().create_update_log(
        'Complete create staff information!', 'Staff')

    return render(request, index_url, staff, status=200)


def info_update(request, staff_id):
    index_url = str(BASE_DIR) + '/staff/template/user/update.html'

    if not staff_id:
        info_url = str(BASE_DIR) + '/staff/template/user/info.html'
        return render(request, info_url, {'message': 'Not found!'}, status=404)

    record_staff = User.objects.filter(id=staff_id).first()

    if not record_staff:
        return render(request, index_url, {'message': 'User does not exist!'}, status=400)

    staff = format_data_staff(record_staff)

    record_log = Log().create_update_log('Display staff information update!', 'Staff')

    return render(request, index_url, staff, status=200)


def display_log(request):
    log_url = str(BASE_DIR) + '/staff/template/log/index.html'

    records_log = Log.objects.all()

    data_log = []
    if records_log:
        for data in records_log:
            data = {
                'content': data.content,
                'time': data.created_at,
                'model': data.model
            }

            data_log.append(data)

    log = {
        'log': data_log
    }

    record_log = Log().create_update_log('Display all log!', 'Log')

    return render(request, log_url, log, status=200)
