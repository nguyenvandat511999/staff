from typing_extensions import Required
import uuid
import time
from django.db import models


# Create your models here.
class User(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, auto_created=True)
    name = models.CharField(max_length=200)
    gender = models.CharField(max_length=100)
    avatar = models.CharField(max_length=500, null=True)
    created_at = models.CharField(
        default=time.strftime("%a, %d %b %Y %H:%M:%S"), max_length=200)

    def __str__(self):
        return self.name

    def create_update_user(self, name, gender, avatar=None):
        self.name = name
        self.gender = gender
        self.avatar = avatar
        self.save()
        return self


class Log(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, auto_created=True)
    created_at = models.CharField(
        default=time.strftime("%a, %d %b %Y %H:%M:%S"), max_length=200)
    content = models.CharField(max_length=500)
    model = models.CharField(max_length=100)

    def create_update_log(self, content, model):
        self.content = content
        self.model = model
        self.save()
        return self
