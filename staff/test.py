import staff
from .models import User
from django.test import TestCase, Client, SimpleTestCase


# Create your tests here.
class UserTestCase(TestCase):
    def setUp(self):
        '''This should never execute but it does when I test test_store_a'''
        self.data = None

    # def test_create_staff(self):
    #     with open('/home/dat/Downloads/0812_wp4676582-4k-pc-wallpapers.jpg', 'rb') as img:
    #         data_staff = {
    #             'name': 'Nguyen Van Dat',
    #             'gender': 'male',
    #             'avatar': img
    #         }

    #         response = self.client.post('/staff/submit', data_staff)

    #         print(response.status_code)
    #         print(response.context['staff'])
    #         self.assertEqual(response.status_code, 200)

    def test_staff(self):
        staff_id = ''
        print('\nCreate information staff!')
        with open('/home/dat/Downloads/0812_wp4676582-4k-pc-wallpapers.jpg', 'rb') as img:
            data_staff = {
                'name': 'Nguyen Van Dat',
                'gender': 'male',
                'avatar': img
            }

            response = self.client.post('/staff/submit', data_staff)

            print(response.status_code)
            print(response.context['staff'])
            staff_id = response.context['staff']['id']
            self.assertEqual(response.status_code, 200)

        print('\nUpdate information staff!')
        with open('/home/dat/Downloads/0812_wp4676582-4k-pc-wallpapers.jpg', 'rb') as img:
            data_staff = {
                'name': 'Nguyen Van Hung',
                'gender': 'female',
                'avatar': img
            }

            response = self.client.post(
                '/staff/update/' + str(staff_id), data_staff)

            print(response.status_code)
            print(response.context['staff'])
            self.assertEqual(response.status_code, 200)

        print('\nDisplay information staff!')
        response = self.client.get(
            '/staff/info/' + str(staff_id))

        print(response.status_code)
        print(response.context['staff'])
        self.assertEqual(response.status_code, 200)

        print('\nDisplay information staff update!')
        response = self.client.get(
            '/staff/info_update/' + str(staff_id))

        print(response.status_code)
        print(response.context['staff'])
        self.assertEqual(response.status_code, 200)

    def test_create_staff_not_name(self):
        print('\nCreate information staff not name!')
        with open('/home/dat/Downloads/0812_wp4676582-4k-pc-wallpapers.jpg', 'rb') as img:
            data_staff = {
                'gender': 'male',
                'avatar': img
            }

            response = self.client.post('/staff/submit', data_staff)

            print(response.status_code)
            print(response.context['message'])
            self.assertEqual(response.status_code, 400)

    def test_create_staff_not_gender(self):
        print('\nCreate information staff not gender!')
        with open('/home/dat/Downloads/0812_wp4676582-4k-pc-wallpapers.jpg', 'rb') as img:
            data_staff = {
                'name': 'Nguyen Van Dat',
                'avatar': img
            }

            response = self.client.post('/staff/submit', data_staff)

            print(response.status_code)
            print(response.context['message'])
            self.assertEqual(response.status_code, 400)

    def test_create_staff_not_avatar(self):
        print('\nCreate information staff not avatar!')
        data_staff = {
            'name': 'Nguyen Van Dat',
            'gender': 'male'
        }

        response = self.client.post('/staff/submit', data_staff)

        print(response.status_code)
        print(response.context['message'])
        self.assertEqual(response.status_code, 400)

    def test_create_staff_with_invalid_avatar(self):
        print('\nCreate information staff with invalid avatar!')
        with open('/home/dat/Documents/bao-cao-cong-viec-14-12.odt', 'rb') as img:
            data_staff = {
                'name': 'Nguyen Van Hung',
                'gender': 'female',
                'avatar': img
            }

            response = self.client.post('/staff/submit', data_staff)

            print(response.status_code)
            print(response.context['message'])
            self.assertEqual(response.status_code, 400)

    def test_update_staff_wrong_id(self):
        print('\nUpdate information staff with staff id is wrong!')
        with open('/home/dat/Downloads/0812_wp4676582-4k-pc-wallpapers.jpg', 'rb') as img:
            data_staff = {
                'name': 'Nguyen Van Hung',
                'gender': 'female',
                'avatar': img
            }

            staff_id = '8c8477234f384c55a53acf7403236846'

            response = self.client.post(
                '/staff/update/' + str(staff_id), data_staff)

            print(response.status_code)
            print(response.context['message'])
            self.assertEqual(response.status_code, 400)

    def test_update_staff_empty_id(self):
        print('\nUpdate information staff with staff id is empty!')
        with open('/home/dat/Downloads/0812_wp4676582-4k-pc-wallpapers.jpg', 'rb') as img:
            data_staff = {
                'name': 'Nguyen Van Hung',
                'gender': 'female',
                'avatar': img
            }

            response = self.client.post(
                '/staff/update/', data_staff)

            print(response.status_code)
            self.assertEqual(response.status_code, 404)

    def test_display_info_staff_with_wrong_id(self):
        print('\n Display information staff update with staff id is empty!')
        staff_id = '8c8477234f384c55a53acf7403236846'

        response = self.client.get(
            '/staff/info_update/' + str(staff_id))

        print(response.status_code)
        self.assertEqual(response.status_code, 400)

    def test_show_log(self):
        print('\n Display all log!')
        response = self.client.get('/staff/log')
        print(response.status_code)
        self.assertEqual(response.status_code, 200)
